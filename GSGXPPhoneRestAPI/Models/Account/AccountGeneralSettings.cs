﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSGXPPhoneRestAPI.Models.Account
{
    public class AccountGeneralSettings
    {
        /// <summary>
        /// "0" is disable, "1" is active.
        /// </summary>
        public string Active { get; set; }
        public string AccountName { get; set; }
        public string SIPServer { get; set; }
        public string SecondarySIPServer { get; set; }
        public string OutboundProxy { get; set; }
        public string BackupOutboundProxy { get; set; }
        public string BLFServer { get; set; }
        public string SIPUserID { get; set; }
        public string AuthenticateID { get; set; }
        public string AuthenticatePassword { get; set; }
        public string Name { get; set; }
        public string VoiceMailAccessNumber { get; set; }
        /// <summary>
        /// "0" is User Name, "1" is User ID.
        /// </summary>
        public string AccountDisplay { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSGXPPhoneRestAPI.Models.Response
{
    public class AuthBodyGSGXPAPIResponse
    {
        public string sid { get; set; }
        public string role { get; set; }
        public bool defaultAuth { get; set; }
    }
    public class AuthGSGXPAPIResponse : BaseGSGXPAPIResponse
    {
        public AuthBodyGSGXPAPIResponse body { get; set; } 
    }
}

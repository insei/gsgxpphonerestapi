﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSGXPPhoneRestAPI.Models
{
    public class GSGXPValuesNames : Dictionary<string, string>
    {
        public Dictionary<int, Dictionary<string, string>> Account;
        public GSGXPValuesNames()
        {
            Account = new Dictionary<int, Dictionary<string, string>>();
            for (int i = 0; i < 30; i++)
            {
                Account.Add(i, new Dictionary<string, string>());
            }
        }
    }
}

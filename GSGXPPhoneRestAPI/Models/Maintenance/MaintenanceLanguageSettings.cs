﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSGXPPhoneRestAPI.Models.Maintenance
{
    public class MaintenanceLanguageSettings
    {
        /// <summary>
        /// "en", "ru", etc
        /// </summary>
        public string DisplayLanguage { get; set; }
        public string LanguageFilePostfix { get; set; }
    }
}

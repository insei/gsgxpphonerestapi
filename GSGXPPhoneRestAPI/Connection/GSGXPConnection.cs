﻿using GSGXPPhoneRestAPI.Connection.Account;
using GSGXPPhoneRestAPI.Data;
using GSGXPPhoneRestAPI.Data.GXP1610;
using GSGXPPhoneRestAPI.Models;
using GSGXPPhoneRestAPI.Models.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GSGXPPhoneRestAPI.Connection
{
    public class GSGXPConnection
    {
        private readonly HttpClient httpClient;
        private readonly GSGXPPhoneModels model;
        public GSGXPValuesNames gxpValuesNames;
        private string username;
        private string password;

        public AccountSettingsManager AccountsSettings;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAdress">Example is 'http://192.168.1.80/'</param>
        /// <param name="username">Username for Web Admin panel</param>
        /// <param name="password">Password for Web Admin panel</param>
        public GSGXPConnection(GSGXPPhoneModels model, string baseAdress, string username, string password)
        {
            this.model = model;
            this.password = password;
            this.username = username;
            httpClient = new HttpClient() { BaseAddress = new Uri(baseAdress) };
            httpClient.DefaultRequestHeaders.Referrer = httpClient.BaseAddress;
            gxpValuesNames = new GSGXPValuesNames();
            AccountsSettings = new AccountSettingsManager(this);

            switch (model)
            {
                case GSGXPPhoneModels.GSGXP1610:
                    GSGXP1610Config.SetValuesNames(gxpValuesNames);
                    break;
                default:
                    break;
            }
        }

        public int SetValuesObj(object obj, Dictionary<string, string> localValuesNames = null)
        {
            var ValuesNames = localValuesNames;
            if (ValuesNames == null)
                ValuesNames = gxpValuesNames;

            var values = new Dictionary<string, string>();
            var objProps = obj.GetType().GetProperties();
            foreach (var settingProp in objProps)
            {
                var value = settingProp.GetValue(obj, null);
                if (value != null)
                {
                    string ValueName;
                    if (!ValuesNames.TryGetValue(settingProp.Name, out ValueName))
                    {
                        Console.WriteLine("Property " + settingProp.Name + " not have a value name");
                        continue;
                    }
                    values.Add(ValueName, (string)value);
                }
            }
            return SetValues(values);
        }

        public int SetValues(Dictionary<string, string> values)
        {
            var bodyContent = "";
            foreach (var value in values)
            {
                if (bodyContent.Length > 0)
                    bodyContent += "&";

                bodyContent += value.Key + "=" + value.Value;
            }
            var content = new StringContent(bodyContent, Encoding.UTF8, "application/x-www-form-urlencoded");
            var request = new HttpRequestMessage(HttpMethod.Post, "cgi-bin/api.values.post") { Content = content };
            var response = Task.Run(() => httpClient.SendAsync(request)).Result;
            if (response.IsSuccessStatusCode)
            {
                return 1;
            }
            return -1;
        }


        private void SetAuth(AuthGSGXPAPIResponse authResponse)
        {
            var Coocie = "session-role=" + authResponse.body.role + "; session-identity=" + authResponse.body.sid;
            httpClient.DefaultRequestHeaders.Add("Cookie", Coocie);
        }
        public int Authorize()
        {
            var content = new StringContent("username=" + HttpUtility.UrlEncode(username) + "&password=" + HttpUtility.UrlEncode(password), Encoding.UTF8, "application/x-www-form-urlencoded");
            var request = new HttpRequestMessage(HttpMethod.Post, "cgi-bin/dologin");
            request.Content = content;
            var response = Task.Run(() => httpClient.SendAsync(request)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseBody = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                var authResponse = JsonConvert.DeserializeObject<AuthGSGXPAPIResponse>(responseBody);
                SetAuth(authResponse);
                return 1;
            }
            return 0;
        }
    }
}

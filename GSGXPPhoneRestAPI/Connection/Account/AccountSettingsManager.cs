﻿using GSGXPPhoneRestAPI.Models.Account;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace GSGXPPhoneRestAPI.Connection.Account
{
    public class AccountSettingsManager
    {
        private readonly GSGXPConnection connectionManager;
        public AccountSettingsManager(GSGXPConnection connectionManager)
        {
            this.connectionManager = connectionManager;
        }

        public int SetGeneralSettings(AccountGeneralSettings settings, int accountNum = 1)
        {
            accountNum -= 1;
            var AccountValuesNames = connectionManager.gxpValuesNames.Account[accountNum];
            return connectionManager.SetValuesObj(settings, AccountValuesNames);
        }
    }
}

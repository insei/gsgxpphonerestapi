﻿using GSGXPPhoneRestAPI.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GSGXPPhoneRestAPI.Data.GXP1610
{
    static class GSGXP1610Config
    {
        public static void SetValuesNames(GSGXPValuesNames gxpValuesNames)
        {
            Account(gxpValuesNames);
            Maintenance(gxpValuesNames);
        }
        private static void Account(GSGXPValuesNames gxpValuesNames)
        {
            //  Account 1 General Settings params names
            gxpValuesNames.Account[0].Add("Active", "P271");
            gxpValuesNames.Account[0].Add("AccountName", "P270");
            gxpValuesNames.Account[0].Add("SIPServer", "P47");
            gxpValuesNames.Account[0].Add("SecondarySIPServer", "P2312");
            gxpValuesNames.Account[0].Add("OutboundProxy", "P48");
            gxpValuesNames.Account[0].Add("BackupOutboundProxy", "P2333");
            gxpValuesNames.Account[0].Add("BLFServer", "P2375");
            gxpValuesNames.Account[0].Add("SIPUserID", "P35");
            gxpValuesNames.Account[0].Add("AuthenticateID", "P36");
            gxpValuesNames.Account[0].Add("AuthenticatePassword", "P34");
            gxpValuesNames.Account[0].Add("Name", "P3");
            gxpValuesNames.Account[0].Add("VoiceMailAccessNumber", "P33");
            gxpValuesNames.Account[0].Add("AccountDisplay", "P2380");
        }
        private static void Maintenance(GSGXPValuesNames gxpValuesNames)
        {
            // Language
            gxpValuesNames.Add("DisplayLanguage", "P1362");
            gxpValuesNames.Add("LanguageFilePostfix", "P399");
        }
    }
}

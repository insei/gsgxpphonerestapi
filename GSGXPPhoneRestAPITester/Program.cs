﻿using GSGXPPhoneRestAPI.Connection;
using GSGXPPhoneRestAPI.Data;
using GSGXPPhoneRestAPI.Models.Account;
using GSGXPPhoneRestAPI.Models.Maintenance;
using System;
using System.Threading.Tasks;

namespace GSGXPPhoneRestAPITester
{
    class Program
    {

        static void Main(string[] args)
        {
            var connection = new GSGXPConnection(GSGXPPhoneModels.GSGXP1610, "http://FPC13/", "admin", "Jsu&ujsd9gHJs");
            var resultAuth = connection.Authorize();
            if (resultAuth > 0)
            {
                Console.WriteLine("Authorization is complete");
                var sipSettings = new AccountGeneralSettings()
                {
                    Active = "1",
                    AccountName = "NewAccount",
                    SIPServer = "Server",
                    SecondarySIPServer = "SecServer",
                    OutboundProxy = "OutboundProxy",
                    BackupOutboundProxy = "BackupOutboundProxy",
                    BLFServer = "BLFServer",
                    SIPUserID = "SIPUserID",
                    AuthenticateID = "AuthenticateID",
                    AuthenticatePassword = "AuthenticatePassword",
                    Name = "Name",
                    VoiceMailAccessNumber = "VoiceMailAccessNumber",
                    AccountDisplay = "0"
                };
                var result = Task.Run(() => connection.AccountsSettings.SetGeneralSettings(sipSettings, 1)).Result;
                if (result > 0)
                    Console.WriteLine("Account general settings is successfully setted");
            }
            var langSettings = new MaintenanceLanguageSettings()
            {
                DisplayLanguage = "en"
            };
            var resultLang = connection.SetValuesObj(langSettings);
            if (resultLang > 0)
                Console.WriteLine("Maintenance language settings is successfully setted");

            Console.ReadLine();
        }
    }
}

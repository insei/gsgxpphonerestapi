# GSGXPPhoneRestAPI

Simple GrandStream GXP Phones Rest API

SetValues function is working for all GXP Phones, but ValuesNames setted only for GXP1610, therefore functions other than SetValues ​​() will not work for other models, without setted ValuesNames.

ValuesNames for GXP1610 setted in:
https://gitlab.com/insei/gsgxpphonerestapi/-/blob/master/GSGXPPhoneRestAPI/Connection/GSGXPConnection.cs#L45

This ValuesNames used only for set Account(SIP) General settings on phone.